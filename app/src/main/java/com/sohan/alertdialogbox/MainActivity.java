package com.sohan.alertdialogbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {


    Button button1, button2, button3, button4, button5, button6, button7, button8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.button:
                //<code>
                Toast.makeText(getApplicationContext(), "Button 1 pressed", Toast.LENGTH_SHORT).show();
                aleartDialogBoxOne();

                break;
            case R.id.button2:
                //<code>
                Toast.makeText(getApplicationContext(), "Button 2 pressed", Toast.LENGTH_SHORT).show();
                aleartDialogBoxTwo();
                break;
            case R.id.button3:
                //<code>
                aleartDialogBoxThree();
                break;
            case R.id.button4:
                //<code>
                aleartDialogBoxFour();
                break;
            case R.id.button5:
                //<code>
                String title = "Make Choice ...";
                String message = "Do you really want to Quit ?";
                aleartDialogBoxFive(title, message);
                break;
            case R.id.button6:
                //<code>
                aleartDialogBoxSix();
                break;

            case R.id.button7:
                //<code>
                Show_Dialog();
                break;
            case R.id.button8:
                //<code>
                showAlert();
                break;

        }
    }

    private void initialization() {
        button1 = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);

    }


    //Dialog fragment
    public void aleartDialogBoxOne() {
        // Toast.makeText(getApplicationContext(), "Entered aleartDialogBoxOne", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //  Toast.makeText(getApplicationContext(), "OK ", Toast.LENGTH_SHORT).show();
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // finish();
            }
        });
        alertDialog.setCancelable(true);
        alertDialog.create();
        alertDialog.show();
    }

    //List dialog
    public void aleartDialogBoxTwo() {
        // Toast.makeText(getApplicationContext(), "Entered aleartDialogBoxOne", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Pick a Color")
                .setItems(R.array.colors_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                })
                .create()
                .show();
    }


    //Single-choice list dialog
    public void aleartDialogBoxThree() {
        // Toast.makeText(getApplicationContext(), "Entered aleartDialogBoxOne", Toast.LENGTH_SHORT).show();

        final ArrayList mSelectedItems = new ArrayList();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("This is list choice dialog box");
        alertDialog.setMultiChoiceItems(R.array.toppings, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked) {
                    // If the user checked the item, add it to the selected items
                    mSelectedItems.add(which);
                } else if (mSelectedItems.contains(which)) {
                    // Else, if the item is already in the array, remove it
                    mSelectedItems.remove(Integer.valueOf(which));
                }
            }
        });

        // Set the action buttons
        alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK, so save the mSelectedItems results somewhere
                // or return them to the component that opened the dialog

            }
        });

        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        alertDialog.create();
        alertDialog.show();
    }

    //Custom Dialog
    public void aleartDialogBoxFour() {
        // Toast.makeText(getApplicationContext(), "Entered aleartDialogBoxOne", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("WARNING !")
                .setMessage("You are not authorised to see this file. \nSORRY!!")
                .setIcon(R.drawable.warning)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //user pressed OK
                    }
                })
//                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        //user pressed cancel
//                    }
//                })
                .create()
                .show();
    }

    //Custom Dialog 2
    public void aleartDialogBoxFive(String title, String message) {
        // Toast.makeText(getApplicationContext(), "Entered aleartDialogBoxOne", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle(title)
                .setMessage(message)
                .setIcon(R.drawable.question_mark)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //user pressed OK
                        //   finish();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //user pressed cancel
                    }
                })
                .create()
                .show();
    }

    //Three Button to AlertDialog Box
    public void aleartDialogBoxSix() {
        // Toast.makeText(getApplicationContext(), "Entered aleartDialogBoxOne", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Save File")
                .setMessage("Do you want to save this file ?")
                .setIcon(R.drawable.save_file)
                .setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //user pressed cancel
                    }
                })
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //user pressed OK

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //user pressed cancel
                    }
                })

                .create()
                .show();
    }

    //Final Custom Dialog BOX :D
    public void Show_Dialog() {
        CustomDialogClass cdd = new CustomDialogClass(MainActivity.this);
        cdd.show();
    }


    //Final Custom Dialog BOX_2
    public void showAlert() {
        ViewDialog alert = new ViewDialog();
        String msg = "ACCESS DENIED!";
        alert.showDialog(MainActivity.this, msg);
    }


}
